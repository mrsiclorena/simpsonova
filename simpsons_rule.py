import math
from mpi4py import MPI
import numpy as np
import time


comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

# S = dx / 3 * ( y[0] + y[-1] + 4 * np.sum(y[1::2]) + 2 * np.sum(y[2::2]))

x_send = None

def f(x):
    return 1 / x
    #return x/2 * 5 + x**2

if rank == 0:
    print("Izracun simpsonove formule: ")
    print("Unesite vrijednost donje granice:")
    a = int(input())
    print("Unesite vrijednost gornje granice:")
    b = int(input())
    print("Unesite vrijednost N (mora biti paran broj): ")
    N = int(input())


    if N%2 ==1:
        raise ValueError("N mora biti paran.")

    time_p = time.time()
    dx = (b - a)/N
    x = np.linspace(a, b, N+1)

    # nije moguce staviti proizvoljan broj procesa uz ovo, poremeti se cijeli poredak
    # if rank == 0 or rank == size - 1:
    #    coeff = 1
    # elif rank % 2 == 0:
    #    coeff = 2
    # else:
    #    coeff = 4

    #zato sam podijelila broj procesa te svakom poslala dio, osim prvog i zadnjeg
    #prvi i zadnji se mnoze s 1, a parni i neparni s 2 i 4
    length = int(N/size)
    x_send = [x[1:-1][i*length:(i+1) * length]for i in range(size)]

else:
    a = None
    b = None
    N = None

#comm.Barrier()

x_data = comm.scatter(x_send, root=0)

y = f(x_data)
#zbog toga sto sam poslala sve osim prvog i zadnjeg, parni indeksi postaju neparni, a neparni parni
# S = dx / 3 * ( y[0] + y[-1] + 4 * np.sum(y[1::2]) + 2 * np.sum(y[2::2]))
zbroj_dio = 4* np.sum(y[::2]) + 2 * np.sum(y[1::2])
zbroj_uk = comm.reduce(zbroj_dio, op= MPI.SUM, root=0)


if rank == 0:
    #prvi i zadnji coef 1
    zbroj_uk += f(x[0]) + f(x[-1])
    S = dx/3 * zbroj_uk

    time_k = time.time()
    vrijeme_f = time_k - time_p
    print("Aproksimacija dodane funkcije =", S, "\nVrijeme izvođenja:", vrijeme_f, "sec")
