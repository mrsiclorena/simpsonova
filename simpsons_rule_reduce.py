import math
from mpi4py import MPI
import numpy as np
import scipy.integrate as spi
import time


comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

zbroj_uk = 0


#def simps(f,a,b,N=50):
#    if N % 2 == 1:
#       raise ValueError("N mora biti paran.")
#       dx = (b-a)/N
#       x = np.linspace(a,b,N+1)
#       y = f(x)
#       S = dx/3 * np.sum(y[0:-1:2] + 4*y[1::2] + y[2::2])
#       return S

if rank == 0:
    print("Izracun simpsonove formule: ")
    print("Unesite vrijednost donje granice:")
    a = int(input())
    print("Unesite vrijednost gornje granice:")
    b = int(input())
    print("Unesite vrijednost N (mora biti paran broj): ")
    N = int(input())
else:
    a = None
    b = None
    N = None

a = comm.bcast(a, root=0)
b = comm.bcast(b, root=0)
N = comm.bcast(N, root=0)

comm.Barrier()

if rank == 0:
    time_p = time.time()
    if N%2 ==1:
        raise ValueError("N mora biti paran.")
    dx = (b - a)/N
    x = np.linspace(a,b,N+1)
    y=1/x

    zbroj_1 = np.array((0.0,0.0,0.0))

    #provjera
    #print('y na pocetku :',y ,'\n')
    comm.send(y, dest=1,tag=11)
    comm.send(y, dest=2,tag=12)
    comm.send(y, dest=3,tag=13)

    #recvmsg1 = comm.recv(source=1)
    #recvmsg2 = comm.recv(source=2)
    #recvmsg3 = comm.recv(source=3)

    #S = dx/3 * np.sum(recvmsg1 +recvmsg2 +recvmsg3)
    
elif rank == 1:
    zbroj_uk = None
    y_1 = comm.recv(source=0,tag=11)
    #provjera
    #print('prosljedeni y' ,y_1, '\n')

    zbroj_1 = y_1[0:-1:2]

elif rank == 2:
    zbroj_uk = None
    y_2 =comm.recv(source=0,tag=12)

    zbroj_1 = 4*y_2[1::2]

elif rank == 3:
    zbroj_uk = None
    y_3 = comm.recv(source=0,tag=13)
    zbroj_1 = y_3[2::2]

zbroj_uk = comm.reduce(zbroj_1, op=MPI.SUM, root=0)

if rank == 0:
    #print(zbroj_uk)
    S= dx/3 * np.sum(zbroj_uk)
    time_k = time.time()
    #vrijeme_f = time_k-time_p
    print("Aproksimacija dodane funkcije =",S,"\nVrijeme izvođenja:",time_k-time_p,"sec")