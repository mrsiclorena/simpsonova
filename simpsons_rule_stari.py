import math
import numpy as np
from mpi4py import MPI
import scipy.integrate as spi
import time


comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

def simps(f,a,b,N=50):
    if N % 2 == 1:
        raise ValueError("N mora biti paran.")
    dx = (b-a)/N
    x = np.linspace(a,b,N+1)
    y = f(x)
    #print('y[0:-1:2]',y[0:-1:2])
    S = dx/3 * np.sum(y[0:-1:2] + 4*y[1::2] + y[2::2])
    return S

if rank == 0:
    print("Izracun simpsonove formule: ")
    print("Unesite vrijednost donje granice:")
    a = int(input())
    print("Unesite vrijednost gornje granice:")
    b = int(input())
    print("Unesite vrijednost N (mora biti paran broj): ")
    N = int(input())
else:
    a = None
    b = None
    N = None

a = comm.bcast(a, root=0)
b = comm.bcast(b, root=0)
N = comm.bcast(N, root=0)

comm.Barrier()

if rank == 0:
    time_p = time.time()
    approximation = simps(lambda x : 1/x,a,b,N)
    #approximation = simps(lambda x: 1/(1+x),a,b,N)
    time_k = time.time()
    vrijeme_f = time_k-time_p
    print("Aproksimacija dodane funkcije =",approximation,"\nVrijeme izvođenja:",vrijeme_f,"sec")

elif rank == 1:
    x = np.linspace(a,b,N+1)
    y = 1/x
    #y = 1/(1+x)
    time_p = time.time()
    approximation = spi.simps(y,x)
    time_k = time.time()
    vrijeme_s = time_k - time_p
    print("Aproksimacija scipy funkcije =",approximation,"\nVrijeme izvođenja:",vrijeme_s,"sec")