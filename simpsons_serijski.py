import math
import numpy as np
import time



def simps(f,a,b,N=50):
    if N % 2 == 1:
        raise ValueError("N mora biti paran.")
    dx = (b-a)/N
    x = np.linspace(a,b,N+1)
    y = f(x)
    #print('y[0:-1:2]',y[0:-1:2])
    S = dx/3 * np.sum(y[0:-1:2] + 4*y[1::2] + y[2::2])
    return S

print("Izracun simpsonove formule: ")
print("Unesite vrijednost donje granice:")
a = int(input())
print("Unesite vrijednost gornje granice:")
b = int(input())
print("Unesite vrijednost N (mora biti paran broj): ")
N = int(input())


time_p = time.time()
approximation = simps(lambda x : 1/x,a,b,N)
#approximation = simps(lambda x: 1/(1+x),a,b,N)
time_k = time.time()
vrijeme_f = time_k-time_p
print("Aproksimacija dodane funkcije =",approximation,"\nVrijeme izvođenja:",vrijeme_f,"sec")
